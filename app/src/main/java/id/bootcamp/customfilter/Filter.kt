package id.bootcamp.customfilter

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Filter (
    var keywordSearch : String = "",
    var sortBy : String = "",
    var sortOrder : String = "AZ",
    var multiOption : List<String> = arrayListOf()
) : Parcelable