package id.bootcamp.customfilter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

class MainActivity : AppCompatActivity() {
    var filter = Filter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_filter) {
            val modalBottomSheet = FilterFragment()
            val bundle = Bundle()
            bundle.putParcelable("data", filter)
            modalBottomSheet.arguments = bundle
            modalBottomSheet.show(supportFragmentManager, FilterFragment::class.java.simpleName)

            modalBottomSheet.onFilterSelected = object  : FilterFragment.OnFilterSelected{
                override fun onFilterSelected(filter: Filter) {
                    this@MainActivity.filter = filter
                }

            }
        }
        return super.onOptionsItemSelected(item)
    }
}