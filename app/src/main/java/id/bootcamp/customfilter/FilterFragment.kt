package id.bootcamp.customfilter

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.view.children
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.chip.Chip
import id.bootcamp.customfilter.databinding.FragmentFilterBinding

class FilterFragment : BottomSheetDialogFragment() {
    private lateinit var binding: FragmentFilterBinding
    var onFilterSelected: OnFilterSelected? = null

    interface OnFilterSelected {
        fun onFilterSelected(filter: Filter)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFilterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val standardBottomSheetBehaviour = BottomSheetBehavior.from(binding.standardBottomSheet)
        standardBottomSheetBehaviour.state = BottomSheetBehavior.STATE_EXPANDED


        val filterData: Filter? = arguments?.getParcelable("data")
        if (filterData != null) {
            binding.tilSearch.editText?.setText(filterData.keywordSearch)

            for (i in 0 until binding.chipUrutanBerdasarkan.childCount - 1) {
                val chip = binding.chipUrutanBerdasarkan.getChildAt(i) as Chip
                if (chip.text.toString() == filterData.sortBy) {
                    chip.isCheckable = true
                }
            }

            for (i in 0 until binding.chipUrutan.childCount - 1) {
                val chip = binding.chipUrutan.getChildAt(i) as Chip
                if (chip.text.toString() == filterData.sortOrder) {
                    chip.isCheckable = true
                }
            }

            for (i in 0 until binding.chipPasien.childCount - 1) {
                val chip = binding.chipPasien.getChildAt(i) as Chip
                if (filterData.multiOption.contains(chip.text.toString())) {
                    chip.isCheckable = true
                }
            }
        }

        binding.btnAturUlang.setOnClickListener {
            binding.tilSearch.editText?.setText("")
            binding.chipUrutanBerdasarkan.check(-1)
            binding.chipUrutan.check(-1)
            binding.chipPasien.check(-1)
        }

        binding.btnTerapkanFilter.setOnClickListener {
            val filter = Filter()

            filter.keywordSearch = binding.tilSearch.editText.toString().trim()

            val selectedUrutkanBerdasarkan = binding.chipUrutanBerdasarkan.checkedChipId
            if (selectedUrutkanBerdasarkan != -1) {
                filter.sortBy =
                    binding.chipUrutanBerdasarkan.findViewById<Chip>(selectedUrutkanBerdasarkan).text.toString()
            }

            val selectedUrutan = binding.chipUrutan.checkedChipId
            if (selectedUrutkanBerdasarkan != -1) {
                filter.sortBy =
                    binding.chipUrutan.findViewById<Chip>(selectedUrutan).text.toString()
            }

            val listPasien = ArrayList<String>()
            val selectedPasien = binding.chipPasien.checkedChipIds
            for (pasienId in selectedPasien) {
                listPasien.add(binding.chipPasien.findViewById<Chip>(pasienId).text.toString())
            }
            filter.multiOption = listPasien
            onFilterSelected?.onFilterSelected(filter)
            dialog?.dismiss()
        }
    }
}